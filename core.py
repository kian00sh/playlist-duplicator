import bs4 as bs
import subprocess
from urllib.parse import unquote
from html import unescape

players = ('Audacious', 'Clementine')

xml_location = str(input("Please enter your xspf location: "))
playlist_name = str(input("Please choose a name for your playlist: "))
player_choice = str(input(
    f"Please choose the player that generated your playlist {players}: "))


def get_addresses():
    with open(xml_location, 'r') as x:
        x = x.read()
        soup = bs.BeautifulSoup(x, 'xml')
        locations = [loc.text for loc in soup.find_all('location')]
        if player_choice == players[0]:
            locations = [(unescape(unquote(l[7:]))) for l in locations]
        elif player_choice == players[1]:
            locations = [(unescape(unquote(l[:]))) for l in locations]
        else:
            print('player not suppurated!')
            exit(0)
        return locations


def copy_files():
    locations = get_addresses()
    subprocess.call(['mkdir', '{}'.format(playlist_name)])
    for loc in locations:
        try:
            subprocess.check_output(
                ['sudo', 'cp', loc, './{}/'.format(playlist_name)])
        except Exception:
            continue


if __name__ == '__main__':
    copy_files()
